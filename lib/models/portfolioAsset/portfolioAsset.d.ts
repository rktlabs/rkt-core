export declare class PortfolioAsset {
    static serialize(req: any, portfolioId: string, data: any): any;
    static serializeCollection(req: any, portfolioId: string, data: any): any;
}
