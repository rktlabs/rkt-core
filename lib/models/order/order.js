"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Order = void 0;
const luxon_1 = require("luxon");
const idGenerator_1 = require("../../util/idGenerator");
const errors_1 = require("../../errors");
const orderSerializer_1 = require("./orderSerializer");
const orderValidator_1 = require("./orderValidator");
class Order {
    constructor(props) {
        this.createdAt = props.createdAt;
        this.orderId = props.orderId;
        this.assetId = props.assetId;
        this.closedAt = props.closedAt;
        this.filledPrice = props.filledPrice;
        this.filledSize = props.filledSize;
        this.filledValue = props.filledValue;
        this.sizeRemaining = props.sizeRemaining;
        this.portfolioId = props.portfolioId;
        this.orderSide = props.orderSide;
        this.orderSize = props.orderSize;
        this.status = props.status;
        this.state = props.state;
        this.orderType = props.orderType;
        this.orderPrice = props.orderPrice;
        this.events = props.events;
        this.tags = props.tags;
        this.xids = props.xids;
        this.reason = props.reason;
    }
    static newOrder(props) {
        const orderId = props.orderId || `ORDER::${idGenerator_1.generateId()}`;
        const createdAt = luxon_1.DateTime.utc().toString();
        // only use fields we want. ignore others.
        const orderEvent = {
            eventType: 'Created',
            publishedAt: createdAt,
            messageId: orderId,
            nonce: idGenerator_1.generateNonce(),
        };
        const newOrderProps = {
            orderId: orderId,
            createdAt: createdAt,
            orderType: props.orderType || 'market',
            assetId: props.assetId,
            portfolioId: props.portfolioId,
            orderSide: props.orderSide,
            orderSize: props.orderSize,
            status: 'received',
            state: 'open',
            events: [orderEvent],
        };
        // limit order requires orderPrice
        if (props.orderPrice) {
            newOrderProps.orderPrice = props.orderPrice;
        }
        if (props.xids) {
            newOrderProps.xids = props.xids;
        }
        if (props.tags) {
            newOrderProps.tags = props.tags;
        }
        const newEntity = new Order(newOrderProps);
        return newEntity;
    }
    static serialize(req, portfolioId, data) {
        return orderSerializer_1.serialize(req, portfolioId, data);
    }
    static serializeCollection(req, portfolioId, data) {
        return orderSerializer_1.serializeCollection(req, portfolioId, data);
    }
    static validate(jsonPayload) {
        try {
            return orderValidator_1.validate(jsonPayload);
        }
        catch (error) {
            throw new errors_1.ValidationError(error);
        }
    }
}
exports.Order = Order;
