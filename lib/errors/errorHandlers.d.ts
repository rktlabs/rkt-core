export declare const handleApiError: (error: any, req: any, res: any, logger: any) => Promise<void>;
export declare const handleEventError: (error: any, req: any, res: any, logger: any) => Promise<void>;
