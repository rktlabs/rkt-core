import { Event } from './event';
export declare class ErrorEvent extends Event {
    constructor(attributes?: {});
}
export declare class WarningEvent extends Event {
    constructor(attributes?: {});
}
