import { Event } from './event';
export declare class ExchangeOrderEventNew extends Event {
    constructor(attributes?: {});
}
export declare class ExchangeOrderEventCancel extends Event {
    constructor(attributes?: {});
}
