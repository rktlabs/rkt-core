import { Event } from './event';
export declare class TransactionEventError extends Event {
    constructor(attributes?: {});
}
export declare class TransactionEventComplete extends Event {
    constructor(attributes?: {});
}
export declare class TransactionEventNew extends Event {
    constructor(attributes?: {});
}
export declare class TransactionEventPortfolioUpdateEvent extends Event {
    constructor(attributes?: {});
}
