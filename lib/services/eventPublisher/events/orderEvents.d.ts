import { Event } from './event';
export declare class OrderEventFailed extends Event {
    constructor(attributes?: {});
}
export declare class OrderEventComplete extends Event {
    constructor(attributes?: {});
}
export declare class OrderEventFill extends Event {
    constructor(attributes?: {});
}
