// tslint:disable:max-classes-per-file

import { Event } from './event'

export class AssetNewEvent extends Event {
    constructor(attributes = {}) {
        super('AssetNew', attributes)
    }
}
